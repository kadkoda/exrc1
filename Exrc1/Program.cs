﻿using System;
using System.Diagnostics;
using EventLogClass;
using EventLogClass.Design_patterns;
using static System.Console;

namespace Exrc1
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("1.Blink you right eye");
            WriteLine("2.Blink your left eye");
            WriteLine("3.Administration");
            WriteLine("4.Exit");

            
            var user = int.Parse(ReadLine());
            
            while (user != 4)
            {
                switch (user)
                {
                    case (1):
                    {
                       LogGenral.Instance.WriteEntry(new LogEntry {Date = DateTime.Now,
                                                                   Message = "Blink Right eye",
                                                                   Severity = EventLogEntryType.Information });
                        break;
                    }
                    case (2):
                    {
                            LogGenral.Instance.WriteEntry(new LogEntry {Date = DateTime.Now,
                                                                        Message = "Blink Left eye",
                                                                        Severity = EventLogEntryType.Information });
                            break;
                    }
                    case (3):
                    {
                            var list = LogGenral.Instance.ReadEnteries(DateTime.Now);

                            foreach (var entry in list)
                            {
                                Console.WriteLine(entry.Date + " " + entry.Severity + " " + entry.Message);
                            }

                        break;
                    }

                    default:
                    {
                        WriteLine("Worng key");
                        break;
                    }
                }

                WriteLine("1.Blink you right eye");
                WriteLine("2.Blink your left eye");
                WriteLine("3.Administration");
                WriteLine("4.Exit");

                user = int.Parse(ReadLine());  
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exrc1
{  
    class EncryptedCvsFileLog : ILog
    {
        public string strNormalFile = "NormalCsv.csv";

        public void WriteEntry(LogEntry logEntry)
        {
            StringBuilder strDetails = new StringBuilder();

            strDetails.Append(logEntry.EnSeverity.ToString() + "," + logEntry.DtEventDateTime.ToString() + ',' +
                              logEntry.StrMessage + Environment.NewLine);
            ;

            for (int i = 0; i < strDetails.Length; i++)
            {
                strDetails[i] = strDetails[i]++;
            }

            if (!File.Exists(strNormalFile))
            {
                string strHeader = "Severity" + "," + " Date and time" + "," + "Message" + Environment.NewLine;
                File.WriteAllText(strNormalFile, strHeader);
            }

            File.AppendAllText(strNormalFile, strDetails.ToString());
        }

        public List<LogEntry> ReadEnteries(DateTime dtDateTime)
        {
           StringBuilder sbDescrypt = new StringBuilder();
           string[] strLines = File.ReadAllLines(strNormalFile);

            for (int i = 0; i < strLines.Length; i++)
            {
                sbDescrypt.Append(strLines[i]);
            }

            for (int i = 0; i < sbDescrypt.Length; i++)
            {
                sbDescrypt[i]--;
            }

            List<LogEntry> lsLogEntries = new List<LogEntry>();

            for (int i = 1; i < strLines.Length; i++)
            {
                string[] strInfo = strLines[i].Split(',');

                if (dtDateTime.Equals(Convert.ToDateTime(strInfo[1])))
                {
                   lsLogEntries.Add(new LogEntry((System.Diagnostics.EventLogEntryType)Enum.Parse(typeof(System.Diagnostics.EventLogEntryType), strLines[0]), Convert.ToDateTime(strLines[1]),strLines[2]));
                }
            }

            return lsLogEntries;
        }

        public void ClearLog()
        {
          System.IO.File.Delete(strNormalFile);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exrc1
{  
    class CsvFileLog:ILog
    {
        public string strNormalFile = "NormalCsv.csv";

        public void WriteEntry(LogEntry logEntry)
        {
           
            string strDetails = logEntry.EnSeverity.ToString() + "," + logEntry.DtEventDateTime.ToString() + ',' +
                                   logEntry.StrMessage + Environment.NewLine; ;


            if (!File.Exists(strNormalFile))
            {
                string strHeader = "Severity" + "," + " Date and time" + "," + "Message" + Environment.NewLine;

                File.WriteAllText(strNormalFile, strHeader);
            }

            File.AppendAllText(strNormalFile, strDetails);
        }

        public List<LogEntry> ReadEnteries(DateTime dtDateTime)
        {
          
            string[] strLines = File.ReadAllLines(strNormalFile);
           List<LogEntry> lsLogEntries = new List<LogEntry>();

            for (int i = 1; i < strLines.Length; i++)
            {
                string[] strInfo = strLines[i].Split(',');

                if (dtDateTime.Equals(Convert.ToDateTime(strInfo[1])))
                {
                   lsLogEntries.Add(new LogEntry((System.Diagnostics.EventLogEntryType)Enum.Parse(typeof(System.Diagnostics.EventLogEntryType), strLines[0]), Convert.ToDateTime(strLines[1]),strLines[2]));
                }
            }

            return lsLogEntries;
        }

        public void ClearLog()
        {
          System.IO.File.Delete(strNormalFile);
        }
    }
}

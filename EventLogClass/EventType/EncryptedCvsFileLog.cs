﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace EventLogClass
{
    public class EncryptedCvsFileLog : ILog
    {
        public void WriteEntry(LogEntry entry)
        {
            var details = new StringBuilder();

            if (!File.Exists(ConfigurationManager.AppSettings["FileName"]))
            {
                 details.Append("Severity" + "," + " Date and time" + "," + "Message" + Environment.NewLine);
                
            }

            details.Append(entry.Severity + "," + entry.Date + ',' + entry.Message + Environment.NewLine);

            for (var index = 0; index < details.Length; index++)
            {
                if (details[index] != '\n')
                {
                    details[index]++;
                }
            }

            File.AppendAllText(ConfigurationManager.AppSettings["FileName"], details.ToString());
        }

        public List<LogEntry> ReadEnteries(DateTime dateTime)
        {
            if (!File.Exists(ConfigurationManager.AppSettings["FileName"])) return null;

            var logEntries = new List<LogEntry>();

            var lines = File.ReadAllLines(ConfigurationManager.AppSettings["FileName"]);

            var stringBuilder = new StringBuilder();

            foreach (var currentString in lines)
            {
                stringBuilder.AppendLine(currentString);
            }

            for (var index = 0; index < stringBuilder.Length; index++)
            {
                if (stringBuilder[index] != '\n')
                {
                    stringBuilder[index]++;
                }
            }

            lines = stringBuilder.ToString().Split('\n');

           for (var index = 1; index < lines.Length; index++)
            {
                var info = lines[index].Split(',');

                if (dateTime.Equals(Convert.ToDateTime(info[1])))
                {
                    logEntries.Add(new LogEntry() { Date = DateTime.Parse(info[1]), Message = info[2], Severity = (EventLogEntryType)Enum.Parse(typeof(EventLogEntryType), info[0]) });
                }
            }

            return logEntries;
        }

        public void ClearLog()
        {
            File.Delete(ConfigurationManager.AppSettings["FileName"]);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace EventLogClass
{
    public class EventLog : ILog
    {
        public void WriteEntry(LogEntry entry)
        {
            System.Diagnostics.EventLog.WriteEntry("Exrc1", entry.Message, entry.Severity);
        }

        public List<LogEntry> ReadEnteries(DateTime dateTime)
        {
            var eventLogs = System.Diagnostics.EventLog.GetEventLogs();

            var logEntries =
                from currentEventLog in eventLogs
                from EventLogEntry eventLogEntry in currentEventLog.Entries
                where dateTime.Equals(eventLogEntry.TimeWritten)
                select new LogEntry
                {
                    Date = DateTime.Now,
                    Message = eventLogEntry.Message,
                    Severity = eventLogEntry.EntryType
                };

            return logEntries.ToList();
        }

        public void ClearLog()
        {
            var vEventLog = new System.Diagnostics.EventLog();
            vEventLog.Clear();
        }
    }
}

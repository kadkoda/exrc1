﻿using System;
using System.Collections.Generic;
using System.IO;
using  System.Configuration;
using System.Diagnostics;

namespace EventLogClass
{  
    public class CsvFileLog:ILog
    {
        public void WriteEntry(LogEntry entry)
        {
            var details = entry.Severity + "," + entry.Date + ',' +
                                entry.Message + Environment.NewLine; 


            if (!File.Exists(ConfigurationManager.AppSettings["FileName"]))
            {
                var header = "Severity" + "," + " Date and time" + "," + "Message" + Environment.NewLine;

                File.WriteAllText(ConfigurationManager.AppSettings["FileName"], header);
            }

            File.AppendAllText(ConfigurationManager.AppSettings["FileName"], details);
        }

        public List<LogEntry> ReadEnteries(DateTime dateTime)
        {        
            if (!File.Exists(ConfigurationManager.AppSettings["FileName"])) return null;

            var logEntries = new List<LogEntry>(); 

            var lines = File.ReadAllLines(ConfigurationManager.AppSettings["FileName"]);

            for (var index = 1; index < lines.Length; index++)
            {
                var info = lines[index].Split(',');

                if (dateTime.Equals(Convert.ToDateTime(info[1])))
                {
                    logEntries.Add(new LogEntry() {Date = DateTime.Parse(info[1]), Message = info[2], Severity = (EventLogEntryType)Enum.Parse(typeof(EventLogEntryType),info[0])});
                }
            }

            return logEntries;
        }

        public void ClearLog()
        {
          File.Delete(ConfigurationManager.AppSettings["FileName"]);
        }
    }
}

﻿
using EventLogClass.InterfaceLog;
using EventLogClass.LogType;

namespace EventLogClass.Design_patterns
{
    internal static class LogFactorycs
    {

        public static ILog CreateLog(LogTypes selectedlog)
        {
            ILog logtypetocreat = null;

            switch (selectedlog)
            {
                case LogTypes.Csv: logtypetocreat = new CsvFileLog();
                    break;
                case LogTypes.Protected:
                    logtypetocreat = new EncryptedCvsFileLog();
                    break;
                case LogTypes.Eventvwr:
                    logtypetocreat = new EventLog();
                    break;
            }

            return logtypetocreat;
        }
    }
}

﻿using System;
using System.Diagnostics;

namespace EventLogClass.Design_patterns
{
    public class LogEntry
    {
        public EventLogEntryType Severity { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
    }
}

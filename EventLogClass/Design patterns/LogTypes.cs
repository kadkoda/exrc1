namespace EventLogClass.Design_patterns
{
    public enum LogTypes
    {
        Csv = 0,
        Protected = 1,
        Eventvwr = 2
    }
}
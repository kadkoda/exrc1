﻿using System;
using System.Collections.Generic;
using System.Text;
using EventLogClass.Design_patterns;

namespace EventLogClass.LogType
{
    public class EncryptedCvsFileLog : CsvFileLog
    {
        public override void WriteEntry(LogEntry entry)
        {
            var strbuilder = new StringBuilder();
            strbuilder.AppendLine(entry.Message);

            for (var index = 0; index < strbuilder.Length - 2; index++)
            {
                strbuilder[index]++;
            }

            entry.Message = strbuilder.ToString();

            base.WriteEntry(entry);
        }

        public override List<LogEntry> ReadEnteries(DateTime dateTime)
        {
            var entries= base.ReadEnteries(dateTime);
            var stringtochange = new StringBuilder();

            foreach (var currententry in entries)
            {
                stringtochange.AppendLine(currententry.Message);

                for (var index = 0; index < stringtochange.Length - 2; index++)
                {
                    stringtochange[index]--;
                }

                currententry.Message = stringtochange.ToString();
                stringtochange.Clear();
            }

            return entries;
        }
    }
}

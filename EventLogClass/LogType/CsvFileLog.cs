﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using EventLogClass.Design_patterns;
using EventLogClass.ExceptionLog;
using EventLogClass.InterfaceLog;

namespace EventLogClass.LogType
{  
    public class CsvFileLog:ILog
    {
        public virtual void WriteEntry(LogEntry entry)
        {
            if (ConfigurationManager.AppSettings["FileName"] == null)
            {
                throw new NoLogDefinedException();
            }
            else       
            { 
                var details = $"{entry.Severity},{entry.Date},{entry.Message} \n";

                if (!File.Exists(ConfigurationManager.AppSettings["FileName"]))
                {
                     var header =  $"{"Severity,Date and time,Message\n"}";

                    File.WriteAllText(ConfigurationManager.AppSettings["FileName"], header);
                }

                File.AppendAllText(ConfigurationManager.AppSettings["FileName"], details);
            }
        }

        public virtual List<LogEntry> ReadEnteries(DateTime dateTime)
        {        
            if (!File.Exists(ConfigurationManager.AppSettings["FileName"])) return null;

            var logEntries = new List<LogEntry>(); 

            var lines = File.ReadAllLines(ConfigurationManager.AppSettings["FileName"]);

            for (var index = 1; index < lines.Length; index++)
            {
                var info = lines[index].Split(',');

                if ((info[0] != " ") && (dateTime >= DateTime.Parse(info[1])))
                {
                    logEntries.Add(new LogEntry() {Date = DateTime.Parse(info[1]), Message = info[2], Severity = (EventLogEntryType)Enum.Parse(typeof(EventLogEntryType),info[0])});
                }
            }

            return logEntries;
        }

        public virtual void ClearLog()
        {
          File.Delete(ConfigurationManager.AppSettings["FileName"]);
        }
    }
}

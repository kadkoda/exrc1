﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using EventLogClass.Design_patterns;
using EventLogClass.InterfaceLog;

namespace EventLogClass.LogType
{
    public class EventLog : ILog
    {
        public void WriteEntry(LogEntry entry)
        {
            if (!System.Diagnostics.EventLog.SourceExists("Exrc1"))
            {
                System.Diagnostics.EventLog.CreateEventSource("MySource", "MyNewLog");
            }

            System.Diagnostics.EventLog.WriteEntry("Exrc1", entry.Message, entry.Severity);
        }

        public List<LogEntry> ReadEnteries(DateTime dateTime)
        {
            var eventLogs = System.Diagnostics.EventLog.GetEventLogs();

            var logEntries =
                from currentEventLog in eventLogs
                from EventLogEntry eventLogEntry in currentEventLog.Entries
                where dateTime.Equals(eventLogEntry.TimeWritten)
                select new LogEntry
                {
                    Date = DateTime.Now,
                    Message = eventLogEntry.Message,
                    Severity = eventLogEntry.EntryType
                };

            return logEntries.ToList();
        }

        public void ClearLog()
        {
            var vEventLog = new System.Diagnostics.EventLog( );
            vEventLog.Clear();
        }
    }
}

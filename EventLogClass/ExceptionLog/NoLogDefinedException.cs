﻿using System;

namespace EventLogClass.ExceptionLog
{
    public class NoLogDefinedException:Exception
    {
        public NoLogDefinedException():base("No log config defined")
        {
        }
    }
}

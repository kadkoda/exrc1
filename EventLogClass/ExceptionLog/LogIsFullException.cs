﻿using System;

namespace EventLogClass.ExceptionLog
{
    public class LogIsFullException:Exception
    {
        public LogIsFullException() : base("Log is full")
        {
        }
    }
}

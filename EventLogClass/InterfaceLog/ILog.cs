﻿using System;
using System.Collections.Generic;
using EventLogClass.Design_patterns;

namespace EventLogClass.InterfaceLog
{
    public interface ILog
    {
        void WriteEntry(LogEntry entry);
        List<LogEntry> ReadEnteries(DateTime dateTime);
        void ClearLog();
    }

}

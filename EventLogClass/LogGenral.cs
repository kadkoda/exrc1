﻿using System;
using System.Collections.Generic;
using System.Configuration;
using EventLogClass.Design_patterns;
using EventLogClass.InterfaceLog;

namespace EventLogClass
{
    public class LogGenral:ILog
    {
         ILog logfile =
            LogFactorycs.CreateLog((LogTypes) Enum.Parse(typeof (LogTypes), ConfigurationManager.AppSettings["LogType"]));

        private LogGenral()
        {
            
        }

        public static LogGenral Instance => SingleNested.instance;

        class SingleNested
        {
            static SingleNested()
            {
                
            }
                internal static readonly LogGenral instance = new LogGenral(); 
        }


        public void WriteEntry(LogEntry entry)
        {

            logfile.WriteEntry(entry);
        }

        public List<LogEntry> ReadEnteries(DateTime dateTime)
        {
            var lsLogEntries = logfile.ReadEnteries(dateTime);

            return lsLogEntries;
        }

        public void ClearLog()
        {
             logfile.ClearLog();
        }
    }
}

